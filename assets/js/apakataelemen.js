// Apa kata elemen
$("#load-more-btn-element").click(function (e) {
    tracker++;
	load_ake(tracker);
});

function load_ake(tracker) {
    $('.element-loading').show();
    $("#load-more-btn-element").hide();

	var until = tracker * 5 + 5;
	if (until > obj.length)
		until = obj.length;

	for (var i = tracker * 5; i < until; i++) {
		var approvalButton = '';
		var approved = '';
		if (obj[i].approved == 0) {
			approvalButton = '<div class="approval-btn thread-header" id="testimoni-' + obj[i].id + '" style="padding: 0 10px 0 10px"><button class="btn-approve btn btn-xs btn-info fluid" onclick="approveWords(' + obj[i].id + ')">Approve</button></div>';
			approved = 'unapproved';
		}

		var theDiv = '' +
		'<div class="grid-item">' +
			'<div class="col-sm-2 col-xs-12">' +
			    '<div class="image-cropper">' +
			        '<img src="' + base_url + obj[i].pic + '" alt="">' +
			    '</div>' +
			'</div>' +
	        '<div class="col-sm-10 col-xs-12">' +
	            '<div class="col-xs-12 thread ' + approved + '" id="testimoni-' + obj[i].id + '">' +
	                '<div class="col-xs-12">' +
						approvalButton +
	                    '<div class="thread-single">' +
							obj[i].testimony +
	                    '</div>' +
						'<div class="thread-footer text-right">' +
							'&mdash;' + obj[i].name +
						'</div>' +
	                '</div>' +
	            '</div>' +
	        '</div>' +
		'</div>';

		$(".grid").append(theDiv);
	}

	$('.element-loading').hide();

	if (until != obj.length)
		$("#load-more-btn-element").show();
	else
		$("#load-more-btn-element").hide();

	$('.grid').masonry('reloadItems');
	$('.grid').masonry({
		itemSelector: '.grid-item'
	});

	if (Modernizr.mq('(min-width: 991px)')) {
		var maxheight = $('.threads').height();
		$('.left-bar').css("min-height", maxheight + "px");
	} else {
        $('.left-bar').css("min-height", 0);
    }
}

function approveWords(test) {
	$.post(base_url + 'api/elements/mantapjiwa', {id: test}, function(data){
		$('.approval-btn#testimoni-' + test).hide();
		$('.thread#testimoni-' + test).removeClass('unapproved');
	});
}
