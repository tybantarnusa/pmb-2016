/*****************************************************************
 * Base URL buat convenient develop di localhost
 * ganti ke http://localhost/pmb/index.php/ tiap kalo ngoding di localhost
 * ganti ke http://pmb.cs.ui.ac.id/ kalo udah dideploy
 *****************************************************************/
var base_url = 'http://localhost/pmb/index.php/';

able = true;

setInterval(function(){
    if (Modernizr.mq('(min-width: 991px)')) {
        var maxheight = $('.threads').height() + 20;
        $('.left-bar').css("min-height", maxheight + "px");
        $('.left-bar').css("padding-bottom", "25px");
    } else {
        $('.left-bar').css("min-height", 0);
        $('.left-bar').css("padding-bottom", 0);
    }
},500);

$(document).ready(function(){

	// Responsive
    if (Modernizr.mq('(max-width: 991px)')) {
        $('.left-bar').show();
        $('.btn-left-bar').hide();
        $('.left-bar').css({
            'margin-left': '0px',
            'margin-top': '-10px',
        });
        $('.threads').css({
            'margin-left': '0px',
        });
        $('.right-bar').hide();
        // var maxheight = $('.threads').height();
        $('.left-bar').css("min-height", 0);
    } else {
        $('.btn-left-bar').show();
        $('.right-bar').show();
        able = true;
        $('.btn-left-bar').css('left', '20px');
        $('.btn-left-bar').html('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
        $('.left-bar').css({
            'margin-left': '-350px',
            'margin-top': '0px'
        });
        var maxheight = $('.threads').height();
        $('.left-bar').css("min-height", maxheight + "px");
    }

    $('.foot-cp-btn').click(function(){
        var d = $('.foot-cp-all').css('display');
        if (d == 'none')
            $('.foot-cp-all').slideDown();
        else
            $('.foot-cp-all').slideUp();
    });
    // End of Responsive

	// Left Sidebar
    $('.btn-left-bar').click(function(){
        if (able) {
            able = false;
            var pos = parseInt($('.left-bar').css('margin-left'), 10);
            if (pos < -15) {
                $('.left-bar').show();
                $('.left-bar').animate({
                    'margin-left': '-15px'
                }, 1000, 'easeOutQuint', function() {
                    able = true;
                });
                $(this).html('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>');
                $('.right-bar').hide();
            } else {
                $('.left-bar').animate({
                    'margin-left': '-350px'
                }, 1000, 'easeOutQuint', function(){
                    $('.left-bar').hide();
                    $('.right-bar').fadeIn('fast');
                    able = true;
                });
                $(this).html('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
            }
        }
    });
    // End of Left Sidebar


	// See Submissions Button
    $('.seesubs').click(function(){
        $('.all-submissions').toggle();
    });
    // End of See Submissions Button


	// Calendar
    $('.calendar').fullCalendar({
        events: base_url + 'api/calendar',
        header: {
            left: 'title',
            center: '',
            right: 'prev,next'
        },
        firstDay: 1,
        height: 'auto',
        eventRender: function(event, element) {
            element.prop("title", event.title);
        },
		eventLimit: 3
    });
    // End of Calendar
});


// Responsive
$(window).resize(function() {
    if (Modernizr.mq('(max-width: 991px)')) {
        $('.left-bar').show();
        $('.btn-left-bar').hide();
        $('.left-bar').css({
            'margin-left': '0px',
            'margin-top': '-10px',
            'min-heigth': '0px'
        });
        $('.right-bar').hide();
        $('.threads').css({
            'margin-left': '0px',
        });
    } else {
        $('.btn-left-bar').show();
        $('.right-bar').show();
        able = true;
        $('.btn-left-bar').css('left', '20px');
        $('.btn-left-bar').html('<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
        $('.left-bar').css({
            'margin-left': '-350px',
            'margin-top': '0px'
        });
        $('.threads').css({
            'margin-left': '60px',
        });
        var maxheight = $('.threads').height();
        $('.left-bar').css("min-height", maxheight + "px");
        $('.right-bar').fadeIn('fast');
    }

	var theight = parseInt($('.thread-content').height(), 10);
	if (theight >= 359) {
		$(".fadeout").show();
	} else {
		$(".fadeout").hide();
	}

    if (Modernizr.mq('(min-width: 991px)')) {
        var maxheight = $('.threads').height();
        $('.left-bar').css("min-height", maxheight + "px");
    }
});


// Change time event label for different post categories
$('#category-select').change(function(){
        var cat = $( "#category-select option:selected" ).val();
        if (cat == 1) $('#event-time').hide();
        else if (cat == 2 || cat == 6) { $('#event-time').show();  $('#time-event-label').text('Deadline:'); }
        else if (cat == 3) { $('#event-time').show();  $('#time-event-label').text('Time:'); }
        else if (cat == 4) $('#event-time').hide();
});


// Timepicker
jQuery('#datetimepicker').datetimepicker();

// Archive
$('.ui.accordion')
  .accordion()
;

// Load More
var track_page = 0;
load_contents(track_page);

$("#load-more-btn").click(function (e) {
    track_page++;
    load_contents(track_page);
});

function load_contents(track_page){
    $('.loading').show();
    $("#load-more-btn").hide();

    $.post(base_url + 'api/posts/' + track_page, {}, function(data){

        var obj = JSON.parse(data);
        var until = obj.length < 6 ? obj.length : 5;

        for (var i = 0; i < until; i++) {

			var monthNames = [
			  "January", "February", "March",
			  "April", "May", "June", "July",
			  "August", "September", "October",
			  "November", "December"
			];

			var dateStr = obj[i].time;
			var a = dateStr.split(" ");
			var d = a[0].split("-");
			var t = a[1].split(":");
			var date = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
			var day = date.getDate();
			var monthIndex = date.getMonth();
			var year = date.getFullYear();
			var hour = addZero(date.getHours());
			var minute = addZero(date.getMinutes());

			var timestamp = day + ' ' + monthNames[monthIndex] + ' ' + year + ' ' + hour + ':' + minute;
			var s = '';
			if (obj[i].commentsnum != 1) s = 's';

			var attachmentIcon = '';

			if (obj[i].hasattachments)
				attachmentIcon = '<i data-toggle="tooltip" title="Read more to see the attachment." class="fa fa-paperclip" aria-hidden="true"></i> ';

			var theDiv = '' +
			'<div class="col-xs-12 thread">' +
				'<div class="col-xs-12">' +
					'<div class="row thread-header">' +
						'<div class="col-sm-1 col-xs-12">' +
							'<div class="image-cropper">' +
								'<img src="' + base_url + obj[i].pic + '" alt="' + obj[i].name + '">' +
							'</div>' +
						'</div>' +
						'<div class="col-sm-11 col-xs-12">' +
							'<div class="title">' + attachmentIcon +
								'<a href="' + base_url + 'post/p/' + obj[i].id + '">' + obj[i].title + '</a>' +
							'</div>' +
							'<div class="meta-data">' +
								obj[i].name + ' | ' + timestamp +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="thread-content" id="content-' + obj[i].id + '">' +
						obj[i].content +
					'</div>' +
					'<div class="thread-footer">' +
						'<div class="col-xs-6" style="padding: 0;">' +
							'<a class="read-more" href="' + base_url + 'post/p/' + obj[i].id + '">' + 'Read More</a>' +
						'</div>' +
						'<div class="col-xs-6 text-right" style="padding: 0;">' +
							'<a href="' + base_url + 'post/p/' + obj[i].id + '#comments">' + obj[i].commentsnum + ' Comment' + s + '</a>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>';

			$(".threader").append(theDiv);

            var theight = parseInt($('#content-' + obj[i].id).height(), 10);
            if (theight >= 359) {
        		$('#content-' + obj[i].id).append('<div class="fadeout"></div>');
    		}
        }

		$('[data-toggle="tooltip"]').tooltip();
        $('.loading').hide();
        $("#load-more-btn").show();

        if (Modernizr.mq('(min-width: 991px)')) {
            var maxheight = $('.threads').height() + 20;
            $('.left-bar').css("min-height", maxheight + "px");
            $('.left-bar').css("padding-bottom", "25px");
        } else {
            $('.left-bar').css("min-height", 0);
            $('.left-bar').css("padding-bottom", 0);
        }

		if(obj.length < 6){
            $("#load-more-btn").hide();
        }

    });
}
// End of Load More

// Apa Kata Elemen
$.get(base_url + "api/elementstrings", function(data){
    $(function(){
        $(".element").typed({
            strings: JSON.parse(data),
            typeSpeed: 0,
            loop: true
        });
    });
});

// Replace textarea with ckeditor
CKEDITOR.replace( 'edit' );

// AJAX-y posting comment
function postComment() {
    var name = $('.own-name').text();
    var ava = $('#avatar').attr('src');
    var c = CKEDITOR.instances.edit.getData();
    if (c == '') return;
    var pid = $('#postid').val();
    $.post("comment", {comment: c, postid: pid}, function(data){
        var constructed =   '<div class="comment">' +
                            	'<div class="col-sm-1 col-xs-12 hidden-xs">' +
                            		'<div class="image-cropper">' +
                            			'<img src="' + ava + '" alt="display picture">' +
                            		'</div>' +
                            	'</div>' +
                            	'<div class="col-sm-11 col-xs-12">' +
                            		'<div class="col-xs-12 thread">' +
                            			'<div class="col-xs-12">' +
                            				'<div class="row thread-header">' +
                            					'<div class="col-xs-11">' +
                            						'<div class="title">' +
                            							name +
                            						'</div>' +
                            						'<div class="meta-data">' +
                            							'Just Now' +
                            						'</div>' +
                            					'</div>' +
                            					'<div class="col-xs-1 text-right">' +
                            						'<!--TODO: Best Answer-->' +
                            					'</div>' +
                            				'</div>' +
                            				'<div class="thread-single">' +
                                            	c +
                            				'</div>' +
                            			'</div>' +
                            		'</div>' +
                            	'</div>' +
                            '</div>';
        $('.comment-list').append(constructed);
        var cc = parseInt($('#comment-count').text()) + 1;
        $('#comment-count').text(cc);
        $('#edit').val('');
        CKEDITOR.instances.edit.setData('');
    });
}

// 9:00 to 09:00
function addZero(n) {
    if (n < 10) return '0' + n;
    return n;
}
