var q_page = 0;
load_questions(q_page);

$("#load-more-btn").click(function (e) {
    q_page++;
    load_questions(q_page);
});

function load_questions(q_page){
    $('.loading').show();
    $("#load-more-btn").hide();

    $.post(base_url + 'api/questions/' + q_page, {}, function(data){

        var obj = JSON.parse(data);
        var until = obj.length < 6 ? obj.length : 5;

        for (var i = 0; i < until; i++) {

			var monthNames = [
			  "January", "February", "March",
			  "April", "May", "June", "July",
			  "August", "September", "October",
			  "November", "December"
			];

			var dateStr = obj[i].time;
			var a = dateStr.split(" ");
			var d = a[0].split("-");
			var t = a[1].split(":");
			var date = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
			var day = date.getDate();
			var monthIndex = date.getMonth();
			var year = date.getFullYear();
			var hour = addZero(date.getHours());
			var minute = addZero(date.getMinutes());

			var timestamp = day + ' ' + monthNames[monthIndex] + ' ' + year + ' ' + hour + ':' + minute;
			var s = '';
			if (obj[i].commentsnum != 1) s = 's';

			var theDiv = '' +
			'<div class="col-xs-12 thread">' +
				'<div class="col-xs-12">' +
					'<div class="row thread-header">' +
						'<div class="col-sm-1 col-xs-12">' +
							'<div class="image-cropper">' +
								'<img src="' + base_url + obj[i].pic + '" alt="' + obj[i].name + '">' +
							'</div>' +
						'</div>' +
						'<div class="col-sm-11 col-xs-12">' +
							'<div class="title">' +
								'<a href="' + base_url + 'forum/q/' + obj[i].id + '">' + obj[i].title + '</a>' +
							'</div>' +
							'<div class="meta-data">' +
								obj[i].name + ' | ' + timestamp +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="thread-content" id="content-' + obj[i].id + '">' +
						obj[i].content +
					'</div>' +
					'<div class="thread-footer">' +
						'<div class="col-xs-6" style="padding: 0;">' +
							'<a href="' + base_url + 'post/p/' + obj[i].id + '">' + 'Read More</a>' +
						'</div>' +
						'<div class="col-xs-6 text-right" style="padding: 0;">' +
							'<a href="' + base_url + 'forum/q/' + obj[i].id + '#comments">' + obj[i].commentsnum + ' Answer' + s + '</a>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>';

			$(".forumer").append(theDiv);

            var theight = parseInt($('#content-' + obj[i].id).height(), 10);
            if (theight >= 359) {
        		$('#content-' + obj[i].id).append('<div class="fadeout"></div>');
    		}
        }

		$('.loading').hide();
        $("#load-more-btn").show();
        var maxheight = $('.threads').height();
        $('.left-bar').css("min-height", maxheight + "px");
		if(obj.length < 6){
            $("#load-more-btn").hide();
        }

    });
}
// End of Load More
