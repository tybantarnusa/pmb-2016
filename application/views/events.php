<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

    <?php
        $role = $this->session->userdata('role');
        if ($role == 3 || $role == 4 || $role == 5 || $role == 10) { ?>
        <div class="visible-xs-block visible-sm-block col-xs-12 kasih-gap">
            <a href="<?php if ($page != 'forum') echo site_url('create'); else echo site_url('ask'); ?>" class="btn fluid btn-primary"><span class="glyphicon glyphicon-pencil"></span> New Post</a>
        </div>
    <?php } ?>

    <?php if (sizeof($posts) == 0) { ?>
        <div class="col-xs-12 thread">
            <div class="col-xs-12">
                <div class="row thread-header">
                    <div class="col-xs-11">
                        <div class="title">
                            Belum Ada Acara
                        </div>
                        <div class="meta-data">
                            Untuk saat ini belum ada pemberitahuan lebih lanjut yang berhubungan dengan rangkaian acara PMB.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }

    foreach ($posts as $post) { ?>
    <div class="col-xs-12 thread <?php if ($currenttime > $post->eventtime) echo 'event-passed'; ?>">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-11">
                    <div class="title">
                        <a href="<?= site_url('post/p/'.$post->id); ?>"><?= $post->title; ?></a>
                    </div>
                    <div class="meta-data">
                        <?= date('j F Y H:i',strtotime($post->eventtime)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
