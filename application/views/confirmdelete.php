<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-12">
                    <div class="title">
                        Konfirmasi
                    </div>
                </div>
            </div>
			<div class="thread-content">
				<div class="col-xs-12 kasih-gap">
				Apakah Kamu yakin ingin menghapus post berikut: <a style="color: #7fbbd7;" href="<?= site_url('post/p/'.$post->id); ?>"><?= $post->title; ?></a><br>
				Sekali dihapus, tidak akan bisa dikembalikan lagi.
				</div>
				<div class="col-xs-12 kasih-gap">
					<a class="btn btn-danger" href="<?= site_url('post/delete/'.$post->id.'/true'); ?>">Iya, hapus postingan tersebut.</a>
				</div>
			</div>
			<div class="thread-footer"></div>
        </div>
    </div>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
