<?php $this->view('header'); ?>
<style rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/table.min.css" />
<style>
    .body {
        background: url('http://wallpapercave.com/wp/9OXmmcv.jpg') !important;
    }

    td img {
        height: 50px;
    }
</style>

<div class="col-xs-12 col-md-8 threads">

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-11">
                    <div class="title">
                        U53r5 D4t4b453
                    </div>
                    <div class="meta-data">
                        <table class="ui blue table">
                            <thead>
                                <tr>
                                    <th>p1c</th>
                                    <th>1D</th>
                                    <th>4l145</th>
                                    <th>r0l3</th>
                                    <th>Last Login</th>
                                </tr>
                            </thead><tbody>
                                <?php foreach($users as $user) { ?>
                                <tr>
                                    <td><img src="<?= base_url($user->pic); ?>" alt="<?= $user->name; ?>" height="50" /></td>
                                    <td><?= $user->npm; ?></td>
                                    <td><?= $user->name; ?></td>
                                    <td><?= $user->role; ?></td>
                                    <td><?= $user->lastlogin; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
