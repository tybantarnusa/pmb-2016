<?php $this->view('header'); ?>
<?php $role = $this->session->userdata('role'); ?>
<link rel="stylesheet" href="<?= base_url('assets/css/apakataelemen.css'); ?>" />

<div class="col-xs-12 col-md-8 threads">

	<?php if($role != 2) { ?>
	<?php if (isset($thankyou)) { ?>

		<div class="col-xs-12 alert alert-warning">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?= $thankyou; ?>
		</div>

	<?php } ?>
	<div class="send-testi col-xs-12">
		<h3><strong>Tuliskan Pesan Kamu</strong></h3>
		<div class="notice col-xs-12 kasih-gap">
			<i class="fa fa-exclamation" aria-hidden="true"></i> Pesan yang dikirim akan dimoderasi terlebih dahulu.
		</div>
		<form method="post" action="<?= site_url('elements'); ?>">
		<div class="form-group col-xs-12">
			<textarea type="text" class="form-control" name="testimonial" rows="5" style="resize: none;"></textarea>
		</div>
		<input type="submit" class="btn-creative btn-6 btn-6a fluid" value="Send">
		</form>
	</div>
	<?php } ?>

	<div class="grid col-xs-12">

	</div>

	<div class="col-xs-12 text-center element-loading" style="margin-bottom: 20px;">
        <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
    </div>

    <button id="load-more-btn-element" class="btn-creative btn-1 btn-1e fluid load-more-btn kasih-gap" style="display: none;"><p>
        Load More
    </p></button>
</div>

<?php $this->view('sidebar-and-js'); ?>
<script>
	var tracker = 0;
	var obj = new Array();
	load_ake_init(tracker);

	function load_ake_init(){
	    $('.element-loading').show();
	    $("#load-more-btn-element").hide();

		<?php if ($role == 10 || $role == 3 || $role == 4) { ?>
	    $.post(base_url + 'api/elements/AiCarGattIg', {}, function(data){
			obj = JSON.parse(data);
			load_ake(tracker);
			$('.grid').masonry({
				itemSelector: '.grid-item'
			});
	    });
		<?php } else { ?>
		$.post(base_url + 'api/elements', {}, function(data){
			obj = JSON.parse(data);
			load_ake(tracker);
			$('.grid').masonry({
				itemSelector: '.grid-item'
			});
	    });
		<?php } ?>
	}
</script>
<script src="<?= base_url('assets/js/apakataelemen.js'); ?>"></script>
<?php $this->view('footer-only'); ?>
