<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

	<?php
	$error = $this->session->flashdata('error');
	if ($error != '') { ?>
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Error!</strong> <?= $error ?>
	</div>
	<?php } ?>

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-12">
                    <div class="title kasih-gap">
                        Ask A Question
                    </div>
                </div>
            </div>
            <form method="post" action="<?= site_url('ask/hada'); ?>">
            <div class="editor">
                <div class="form-group">
                    <label for="title">Subject:</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group">
                    <textarea type="text" class="form-control" id="edit" name="content"></textarea>
                </div>

                <input type="submit" class="btn-creative btn-6 btn-6a fluid" value="Post"/>
                </form>
            </div>
        </div>
    </div>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
