<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">
    <?php $role = $this->session->userdata('role'); ?>
    <?php    if ($role != 1 && $role != 9) {    ?>
    <div class="visible-xs-block visible-sm-block col-xs-12 kasih-gap">
        <a href="<?= site_url('ask'); ?>" class="btn fluid btn-primary"><span class="glyphicon glyphicon-pencil"></span> Ask A Question</a>
    </div>
    <?php   }   ?>

    <?php if (sizeof($posts) == 0) { ?>
        <div class="col-xs-12 thread">
            <div class="col-xs-12">
                <div class="row thread-header">
                    <div class="col-xs-11">
                        <div class="title">
                            Tidak Ada Post
                        </div>
                        <div class="meta-data">
                            Belum ada yang melakukan post di forum tanya jawab.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="loading text-center"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></div>
    <div class="forumer">
    </div>

    <?php foreach ($posts as $post) { ?>
        <!-- <div class="col-xs-12 thread">
            <div class="col-xs-12">
                <div class="row thread-header">
                    <div class="col-sm-1 col-xs-12">
                        <div class="image-cropper">
                            <img src="<?= base_url($post->pic); ?>" alt="display picture">
                        </div>
                    </div>
                    <div class="col-sm-11 col-xs-12">
                        <div class="title">
                            <a href="<?= site_url('forum/q/'.$post->id); ?>"><?= $post->title; ?></a>
                        </div>
                        <div class="meta-data">
                            <?= $post->name; ?> | <?= date('j F Y H:i',strtotime($post->time)); ?>
                        </div>
                    </div>
                </div>
                <div class="thread-content">
                    <?= $post->content; ?>
                </div>
                <div class="thread-footer text-right">
                    <a href="<?= site_url('post/p/'.$post->id.'#comments'); ?>"><?= $post->commentsnum; ?> Answer<?php if ($post->commentsnum != 1) echo 's'; ?></a>
                </div>
            </div>
        </div> -->
    <?php } ?>

    <button class="btn-creative btn-1 btn-1e fluid load-more-btn kasih-gap" id="load-more-btn" style="display: none;"><p>
        Load More
    </p></button>

</div>

<?php $this->view('sidebar-and-js'); ?>
<script src="<?= base_url('assets/js/qa_forum.js'); ?>"></script>
<?php $this->view('footer-only'); ?>
