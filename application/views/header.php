<?php
    if (!$this->session->userdata('logged')) redirect(site_url('login'));
    if ($this->session->userdata('role') == 1 && $page != 'please wait') redirect(site_url('wait'));
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= ucwords($page); ?> | PMB Fasilkom UI 2016 #SebuahLangkahAwal</title>
	<meta name="description" content="Sistem informasi Pembinaan Mahasiswa Baru Fasilkom UI 2016. #SebuahLangkahAwal.">
	<meta name="robots" content="index, follow">
	<meta name="author" content="PMB 2016">

	<meta property="og:title" content="<?= ucwords($page); ?> | PMB Fasilkom UI 2016 #SebuahLangkahAwal">
	<meta property="og:site_name" content="PMB Fasilkom UI 2016">
	<meta property="og:url" content="http://pmb.cs.ui.ac.id/">
	<meta property="og:description" content="Sistem informasi Pembinaan Mahasiswa Baru Fasilkom UI 2016. #SebuahLangkahAwal.">
	<meta property="og:type" content="website">
	<meta property="og:image" content="http://pmb.cs.ui.ac.id/assets/images/logo.png">

    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/component.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/default.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/jquery.datetimepicker.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/fullcalendar.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/fullcalendar.print.css" media="print" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/accordion.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<link rel="shortcut icon" type="image/png" href="<?= site_url('assets/images/favicon.png'); ?>"/>
</head>
<body>
    <nav class='navbar navbar-default navbar-fixed-top' role='navigation'>
        <div class='navbar-header'>
            <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navbar-collapse-1'>
                <span class='sr-only'>Toggle Navigation</span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </button>
            <a class='navbar-brand' href='<?= site_url(); ?>'><img class='navbar-image' src='<?= base_url('assets/images/logo.png'); ?>'></a>
        </div>

        <div class='collapse navbar-collapse navbar-right' id='navbar-collapse-1'>
            <ul class='nav navbar-nav'>
                <li <?php if ($page == 'home') echo "class='active' id='active'"; ?>>
                    <a class='navbar-menu' href='<?= site_url(); ?>'>Home</a>
                </li>
                <li <?php if ($page == 'assignments') echo "class='active' id='active'"; ?>>
                    <a class='navbar-menu' href='<?= site_url('assignments'); ?>'>Assignments</a>
                </li>
                <li <?php if ($page == 'events') echo "class='active' id='active'"; ?>>
                    <a class='navbar-menu' href='<?= site_url('events'); ?>'>Events</a>
                </li>
                <li <?php if ($page == 'forum') echo "class='active' id='active'"; ?>>
                    <a class='navbar-menu' href='<?= site_url('forum'); ?>'>Q&amp;A Forum</a>
                </li>
                <!--li <?php if ($page == 'statistic') echo "class='active' id='active'"; ?>>
                    <a class='navbar-menu' href='<?php //site_url('notavailable'); ?>'>Statistik</a>
                </li-->
                <li <?php if ($page == 'apa kata elemen') echo "class='active' id='active'"; ?>>
                    <a class='navbar-menu' href='<?= site_url('elements'); ?>'>Apa Kata Elemen</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="body">

    <div class="col-xs-12 col-md-3 left-bar" >
        <div class="col-xs-12 text-center">
            <div id="profile">
                <div class="image-cropper">
                    <img src="<?= base_url($this->session->userdata('dp')) ?>" alt="<?= $this->session->userdata('name'); ?>" id="avatar">
                </div>
    			<h3 class="own-name"><strong><?= $this->session->userdata('name'); ?></strong></h3>
    			<a href="<?= site_url('profile/edit'); ?>"><h6>Change Display Picture</h6></a>
    			<a href="<?= site_url('logout'); ?>"><h6>Logout</h6></a>
    		</div>

            <div class=' hidden-xs hidden-sm archive' id='archive'>
                <h3>Archives</h3>

                <?php $posts = $this->postsmodel->getAll();
                $_month = '0000-00'; ?>

                <div class="ui styled accordion text-left">
                    <?php foreach ($posts as $post) {
                        if (date('Y-m', strtotime($post->time)) !== $_month) { ?>
                    <?php if ($_month != '0000-00') echo '</div>'; ?>
                    <div class="title">
                        <i class="dropdown icon"></i>
                        <?= date('F Y', strtotime($post->time)); ?>
                    </div>
                    <div class="content" style="font-size: 14px;">
                    <?php } ?>
                        <i class="fa fa-chevron-right" aria-hidden="true" style="font-size: 10px;"></i> <a href="<?= site_url('post/p/'.$post->id); ?>"><?= $post->title; ?></a><br />
                    <?php if (date('Y-m', strtotime($post->time)) !== $_month) { ?>
                    <?php }
                        $_month = date('Y-m', strtotime($post->time));
                    } ?>
                    </div>
    		    </div>
            </div>

            <div class="contact hidden-xs hidden-sm" id="contact-person">
                <h3>Contact Person</h3>
                <div  style="text-align: left;">
                    <strong>Ibad Rahadian Saladdin</strong> - Ketua Pelaksana<br />082112266450<br /><br />
                    <strong>Andhita Nurul Ainun</strong> - Wakil Ketua Pelaksana<br />085216494989<br /><br />
                    <strong>Annissa Fildzah RP</strong> - PJ Akademis<br />081993464909<br /><br />
                    <strong>Ahmad Nazhif RR</strong> - PJ Komisi Disiplin<br />085718484232<br /><br />
                    <strong>Reyneta Carissa A</strong> - PJ Mentor<br />08997344098<br /><br />
                </div>
            </div>
        </div>
    </div>

    <div class="btn btn-default btn-left-bar"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
