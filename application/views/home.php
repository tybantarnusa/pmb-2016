<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

<?php
    $role = $this->session->userdata('role');
    if ($role == 3 || $role == 4 || $role == 5 || $role == 10) { ?>
    <div class="visible-xs-block visible-sm-block col-xs-12 kasih-gap">
        <a href="<?php if ($page != 'forum') echo site_url('create'); else echo site_url('ask'); ?>" class="btn fluid btn-primary"><span class="glyphicon glyphicon-pencil"></span> New Post</a>
    </div>
<?php } ?>

    <div class="threader"></div>

    <div class="col-xs-12 text-center loading" style="margin-bottom: 20px;">
        <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
    </div>

    <button id="load-more-btn" class="btn-creative btn-1 btn-1e fluid load-more-btn kasih-gap" style="display: none;"><p>
        Load More
    </p></button>

</div>

<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
