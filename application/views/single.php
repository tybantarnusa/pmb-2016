<?php
    $this->view('header');
    $role = $this->session->userdata('role');
?>
<div class="col-xs-12 col-md-8 threads">

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header vertical-align">
                <div class="col-sm-1 hidden-xs">
                    <div class="image-cropper">
                        <img src="<?= base_url($post->pic); ?>" alt="display picture">
                    </div>
                </div>
                <div class="col-sm-10 col-xs-11">
                    <div class="title">
                        <a href="#"><?= $post->title; ?></a>
                    </div>
                    <div class="meta-data">
                        <?= $post->name; ?> | <?= date('j F Y H:i',strtotime($post->time)); ?>
                    </div>
                </div>
				<div class="col-xs-1 text-right">
                <?php if ($role != 2 && ($post->author == $this->session->userdata('npm') || $role == 3 || $role == 10)) { ?>
					<a href="<?= site_url('post/delete/'.$post->id); ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
				<?php } ?>
                </div>
            </div>
            <div class="thread-single">
                <?= $post->content; ?>
            </div>
            <?php if ($post->type != 5 && $post->attachment) { ?>
            <div class="thread-attach">
                <span class="glyphicon glyphicon-paperclip"></span> <a href="<?= base_url($post->attachment->link); ?>"><?= $post->attachment->filename; ?></a>
            </div>
            <?php } ?>
        </div>
    </div>

    <?php if (($role == 2 || $role == 3 || $role == 10) && $post->type == 2) { ?>

    <?php
    $error = $this->session->flashdata('error');

    if ($error != '') { ?>
    <div class="col-xs-12">
        <div class="alert alert-danger">
            <strong>Upload Failed!</strong> <?= $error; ?>
        </div>
    </div>
    <?php } ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/icon.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/message.min.css" />
    <div class="col-xs-12 submission thread">
        <div class="col-xs-12">
            <div class="row thread-header text-center kasih-gap">
                <div class="col-xs-12">
                    <div class="title">
                        Submission
                    </div>
                    <div class="meta-data">
                        Deadline: <?= date('j F Y H:i',strtotime($post->eventtime)); ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <?php
                $currenttime = date("Y-m-d H:i:s", time() + 119);
                if ($currenttime <= $post->eventtime) { ?>
                <?= form_open_multipart(site_url('submit')); ?>
                <?php if ($submission) { ?>
                    <div class="ui green message">
                        Your submission: <a href="<?= base_url('uploads/submissions/'.$submission->file); ?>"><?= $submission->file; ?></a>
                    </div>
                <?php } else { ?>
                    <div class="ui warning message">
                        Kamu belum mengumpulkan tugas ini.
                    </div>
                <?php } ?>
                <div class="form-group kasih-gap">
                    <input type="file" name="submission" class="form-control" id="submission" />
                    <input type="hidden" name="postid" value="<?= $post->id; ?>" />
                    <input type="hidden" name="deadline" value="<?= $post->eventtime; ?>" />
                    <span style="color: #666">Allowed files: pdf, zip, rar</span>
                    <input class="form-control btn btn-primary" type="submit" value="Upload" />
                </div>
                <?= form_close(); ?>
                <div class="title" style="color: #666;">
                    <i class="warning icon"></i>File yang diambil adalah yang paling terakhir disubmit.
                </div>
                <?php } else { ?>
                    <?php if ($submission) { ?>
                        <div class="ui green message">
                            Your submission: <a href="<?= base_url('uploads/submissions/'.$submission->file); ?>"><?= $submission->file; ?></a>
                        </div>
                    <?php } else { ?>
                        <div class="ui warning message">
                            Kamu tidak mengumpulkan tugas ini.
                        </div>
                    <?php } ?>
                    <div class="ui negative message text-center">
                        <div class="header">
                            Kamu sudah tidak dapat mengumpulkan tugas ini lagi.
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php if (($role == 5 || $role == 3 || $role == 10) && $post->type == 2) { ?>
        <div class="col-xs-12 thread">
            <div class="col-xs-12">
                <div class="row thread-header">
                    <div class="col-xs-12">
                        <div class="title">
                            <button class="btn btn-primary seesubs"><strong>See Submissions</strong></button>
                        </div>
                    </div>
                </div>
                <div class="all-submissions" style="display: none;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>File</th>
                                <th>Waktu Submit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($post_submissions as $s) { ?>
                            <tr>
                                <td><?= $s->name; ?></td>
                                <td><a href="<?= base_url('uploads/submissions/'.$s->file) ?>"><?= $s->file; ?></a></td>
                                <td><?= date('d/m/Y H:i:s',strtotime($s->time)); ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    <?php } ?>

    <div class="col-xs-12 comments" id="comments">
        <h3><strong><?php echo ($page == 'forum' ? 'Answers' : 'Comments'); ?> (<span id="comment-count"><?= sizeof($comments); ?></span>)</strong></h3>

        <div class="comment-list">
            <?php foreach($comments as $comment) { ?>
            <div class="comment">
                <div class="col-sm-1 col-xs-12 hidden-xs">
                    <div class="image-cropper">
                        <img src="<?= base_url($comment->pic); ?>" alt="display picture">
                    </div>
                </div>
                <div class="col-sm-11 col-xs-12">
                    <div class="col-xs-12 thread">
                        <div class="col-xs-12">
                            <div class="row thread-header">
                                <div class="col-xs-11">
                                    <div class="title">
                                        <?= $comment->name; ?>
                                    </div>
                                    <div class="meta-data">
                                        <?= date('j F Y H:i',strtotime($comment->time)) ?>
                                    </div>
                                </div>
                                <div class="col-xs-1 text-right">
                                    <!--TODO: Best Answer-->
                                </div>
                            </div>
                            <div class="thread-single">
                                <?= $comment->comment; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

        <div class="post-comment col-xs-12">
            <h3><strong>Post Reply</strong></h3>
			<div class="form-group">
                <textarea type="text" class="form-control" id="edit" name="comment"></textarea>
            </div>
            <input type="hidden" id="postid" value="<?= $postId; ?>">
            <button type="button" class="btn-creative btn-6 btn-6a fluid" onclick="postComment()">Post</button>
        </div>
    </div>
</div>

<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
