<div class="col-xs-12 col-sm-4 col-md-3 right-bar">
    <?php
        $role = $this->session->userdata('role');
        if ($role == 3 || $role == 4 || $role == 5 || $role == 10) { ?>
        <div class="col-xs-12 kasih-gap">
            <a href="<?php if ($page != 'forum') echo site_url('create'); else echo site_url('ask'); ?>" class="btn fluid btn-primary"><span class="glyphicon glyphicon-pencil"></span> New Post</a>
        </div>
    <?php }
        else if ($role == 2 && $page == 'forum') { ?>
        <div class="col-xs-12 kasih-gap">
            <a href="<?= site_url('ask'); ?>" class="btn fluid btn-primary"><span class="glyphicon glyphicon-pencil"></span> Ask A Question</a>
        </div>
    <?php } ?>
        <div class="col-xs-12 right-bar-props">
            <div class="col-xs-12" id="kataelemen">
                <div class="text-center title"><h4><strong>Apa Kata Elemen</strong></h4></div>
                <span class="element"></span>
            </div>
            <div class="col-xs-12 clock">
                <script>
    				// Jam copas dari SCeLE

                    // Current Server Time script (SSI or PHP)- By JavaScriptKit.com (http://www.javascriptkit.com)
    				// For this and over 400+ free scripts, visit JavaScript Kit- http://www.javascriptkit.com/
    				// This notice must stay intact for use.

    				//Depending on whether your page supports SSI (.shtml) or PHP (.php), UNCOMMENT the line below your page supports and COMMENT the one it does not:
    				//Default is that SSI method is uncommented, and PHP is commented:

    				//var currenttime = '<!--#config timefmt='%B %d, %Y %H:%M:%S'--><!--#echo var='DATE_LOCAL' -->' //SSI method of getting server date
    				//var currenttime = '<? print date('F d, Y H:i:s', time())?>' //PHP method of getting server date
    				<?php date_default_timezone_set('Asia/Jakarta'); ?>
    				var currenttime = '<?php print date('F d, Y H:i:s', time() + 119)?>' //PHP method of getting server date

                    var montharray=new Array('January','February','March','April','May','June','July','August','September','October','November','December')
                    var serverdate=new Date(currenttime)

                    function padlength(what){
                    var output=(what.toString().length==1)? '0'+what : what
                    return output
                    }

                    function displaytime(){
                    serverdate.setSeconds(serverdate.getSeconds()+1)
                    var datestring=padlength(serverdate.getHours())+':'+padlength(serverdate.getMinutes())+':'+padlength(serverdate.getSeconds())
                    var timestring=('')
                    document.getElementById('servertime').innerHTML=datestring+' '+timestring
                    }

                    window.onload=function(){
                    setInterval('displaytime()', 1000)
                    }
                </script>
                <div class="col-xs-12 text-center" style="padding-top: 10px;">
                    <strong id="servertime"></strong><br />
                    Time
                </div>
            </div>
            <div class="col-xs-12" id="calendar">
                <div class="text-center title"><h4><strong>Calendar</strong></h4></div>
                <div class="calendar"></div>
            </div>
            <div class="col-xs-12" id="medis">
                <div class="ribbon"><span>Medis</span></div>
                <div class="text-center title"><h4><strong>Info Medis</strong></h4></div>
                <span class="medis"><?= $this->medicalmodel->getOneRandomly()->info; ?></span>
            </div>
        </div>
    </div>

    </div>

    <div class="visible-xs-block visible-sm-block clearfix col-xs-12 foot-cp text-center">
        <button class="btn btn-default btn-small foot-cp-btn kasih-gap"><h4><strong>Contact Person</strong></h4></button>
        <div class="foot-cp-all" style="display: none;">
            <strong>Ibad Rahadian Saladin</strong> - Ketua Pelaksana<br />082112266450<br /><br />
            <strong>Andhita Nurul Ainun</strong> - Wakil Ketua Pelaksana<br />085216494989<br /><br />
            <strong>Annissa Fildzah RP</strong> - PJ Akademis<br />081993464909<br /><br />
            <strong>Ahmad Nazhif RR</strong> - PJ Komisi Disiplin<br />085718484232<br /><br />
            <strong>Reyneta Carissa A</strong> - PJ Mentor<br />08997344098<br /><br />
        </div>
    </div>

    <div class="clearfix col-xs-12 footer">
        <div class="col-xs-6 visible-xs-block">Dikembangkan dengan cinta oleh<br />
        <strong>Tim HPK PMB 2016</strong></div>
		<div class="col-xs-4 hidden-xs"></div>
		<div class="col-xs-4 text-center hidden-xs">Dikembangkan dengan cinta oleh<br />
        <strong>Tim HPK PMB 2016</strong></div>
        <div class="col-xs-6 col-sm-4 text-right" style="font-size: 12px;">Support & Bug Report:<br><a href="http://line.me/ti/p/~tybantarnusa" target="_blank">Thoyib Antarnusa</a> | <a href="http://line.me/ti/p/~anabmaulana" target="_blank">Anab Maulana</a></div>
    </div>
<script src="<?= base_url(); ?>assets/js/jquery-3.1.0.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/classie.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.custom.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr-mq.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.autoellipsis-1.0.10.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dotdotdot.min.js"></script>
<script src="<?= base_url(); ?>assets/js/moment.min.js"></script>
<script src="<?= base_url(); ?>assets/js/fullcalendar.min.js"></script>
<script src="<?= base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?= base_url('assets/js/masonry.pkgd.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/typed.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/accordion.min.js'); ?>"></script>
<script src="<?= base_url(); ?>assets/js/script.js"></script>
