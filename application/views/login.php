<!DOCTYPE html>
<html lang="en">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PMB Fasilkom UI 2016 #SebuahLangkahAwal</title>
	<meta name="description" content="Sistem informasi Pembinaan Mahasiswa Baru Fasilkom UI 2016. #SebuahLangkahAwal.">
	<meta name="robots" content="index, follow">
	<meta name="author" content="PMB 2016">

	<meta property="og:title" content="PMB Fasilkom UI 2016 #SebuahLangkahAwal">
	<meta property="og:site_name" content="PMB Fasilkom UI 2016">
	<meta property="og:url" content="http://pmb.cs.ui.ac.id/">
	<meta property="og:description" content="Sistem informasi Pembinaan Mahasiswa Baru Fasilkom UI 2016. #SebuahLangkahAwal.">
	<meta property="og:type" content="website">
	<meta property="og:image" content="http://pmb.cs.ui.ac.id/assets/images/logo.png">

    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/component.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<link rel="shortcut icon" type="image/png" href="<?= site_url('assets/images/favicon.png'); ?>"/>

    <style>
    body {
        font-family: 'Lato', sans-serif;
        background: url('<?= base_url('assets/images/bg_login.jpg'); ?>');
        background-attachment: fixed;
        background-size: cover;
        overflow-x: hidden;
        color: #333;
        height: 0;
    }

    .img-responsive {
        margin: 0 auto;
    }

    a {
        text-decoration: none;
    }

    a:hover {
        text-decoration: none;
    }

    a:visited {
        color: #333;
        text-decoration: none;
    }
    </style>
</head>
<body>
<div style="margin-bottom: 50px;"></div>
<div class="col-xs-12 text-center">
    <img class="img-responsive" src="<?= base_url('assets/images/logo.png'); ?>" alt="logo">
    <p class="perspective">
        <a href="<?= site_url('login/sso'); ?>" class="btn-creative btn-8 btn-8b">Login SSO</a>
    </p>
</div>

<script src="<?= base_url(); ?>assets/js/jquery-3.1.0.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/classie.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr.custom.js"></script>
<script src="<?= base_url(); ?>assets/js/modernizr-mq.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.autoellipsis-1.0.10.min.js"></script>
<script src="<?= base_url(); ?>assets/js/script.js"></script>
</body>
</html>
