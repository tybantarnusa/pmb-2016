<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-11">
                    <div class="title">
                        Info Kesehatan
                    </div>
                </div>
            </div>
            <div class="col-xs-12 thread-content">
                <h4><strong>Tambah Info Kesehatan</strong></h4>
                <form action="" method="post">
                    <div class="form-group">
                        <textarea class="form-control" name="info" rows="5"></textarea>
                    </div>
                    <div class="form-group fluid text-right">
                        <input type="submit" class="btn btn-primary" value="Tambah">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php foreach ($infos as $info) { ?>
    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="col-xs-12 thread-content">
                <?= $info->info; ?>
            </div>
            <div class="col-xs-12 thread-footer">
                <a class="btn btn-danger" href="<?= site_url('medis/delete/'.$info->id); ?>">Delete</a>
            </div>
        </div>
    </div>
    <?php } ?>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
