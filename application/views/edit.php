<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-11">
                    <div class="title">
                        Change Display Picture
                    </div>
                </div>
            </div>
            <div class="thread-content">
                <div class="text-center kasih-gap">
                    <div class="image-cropper">
                        <img src="<?= base_url($this->session->userdata('dp')) ?>" alt="display picture">
                    </div>
                </div>
                <?php echo $error;?>
                <?php echo form_open_multipart('profile/do_upload');?>
                    Gunakan foto dengan orientasi landscape untuk agar ditampilkan dengan baik.
                    <div class="form-group">
                        <input class="form-control" type="file" name="userfile" size="20" />
                    </div>
                    <div class="form-group">
                        <input class="form-control btn btn-info" type="submit" value="Upload Image" />
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
