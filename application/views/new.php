<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

	<?php
	$error = $this->session->flashdata('error');
	if ($error != '') { ?>
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Error!</strong> <?= $error ?>
	</div>
	<?php } ?>

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-12">
                    <div class="title kasih-gap">
                        Create A New Post
                    </div>
                </div>
            </div>
            <?= form_open_multipart(site_url('create/hada')); ?>
            <div class="category">
                <div class="form-group">
                    <label for="category-select">Category:</label>
                    <select class="form-control" id="category-select" name="category">
                        <option value="1">News</option>
                        <?php
                            $role = $this->session->userdata('role');
                            if ($role == 3 || $role == 5 || $role == 10) {
                        ?>
                        <option value="2">Assignment (With Submission Slot)</option>
                        <option value="6">Assignment (No Submission Slot)</option>
                        <?php } ?>
                        <option value="3">Event</option>
                        <option value="4">Other</option>
                    </select>
                </div>
            </div>

            <div class="editor">
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group" id="event-time" style="display: none;">
                    <label for="datetimepicker" id="time-event-label">Time/Deadline:</label>
                    <input type="text" class="form-control" id="datetimepicker" name="event-time">
                </div>
                <div class="form-group">
                    <textarea type="text" class="form-control" id="edit" name="content"></textarea>
                </div>
                <div class="form-group">
                    <label for="attachment">Attachment:</label>
                    <input type="file" name ="attachment" class="form-control" id="attachment" />
                    Allowed types: pdf, zip, rar, 7z
                </div>
                <input type="submit" class="btn-creative btn-6 btn-6a fluid" value="Post"/>
                </form>
            </div>
        </div>
    </div>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
