<?php $this->view('header'); ?>

<div class="col-xs-12 col-md-8 threads">

    <div class="col-xs-12 thread">
        <div class="col-xs-12">
            <div class="row thread-header">
                <div class="col-xs-11">
                    <div class="title">
                        Error 404
                    </div>
                    <div class="meta-data">
                        Halaman yang Kamu tuju tidak ada.
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->view('sidebar-and-js'); ?>
<?php $this->view('footer-only'); ?>
