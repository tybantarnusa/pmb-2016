<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Elementsmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function create() {
        $npm = $this->session->userdata('npm');
        $testimony = strip_tags($this->input->post('testimonial', TRUE));

        if ($testimony == '') return false;

        $this->db->set('author', $npm);
        $this->db->set('testimony', $testimony);
        $this->db->insert('pmb_elements_words');
    }

    function getAll() {
        $this->db->order_by('id', 'desc');
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_elements_words.author');
        return $this->db->get('pmb_elements_words')->result();
    }

    function getApproved() {
        $this->db->where('approved', 1);
        $this->db->order_by('rand()');
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_elements_words.author');
        return $this->db->get('pmb_elements_words')->result();
    }

    function getApprovedStrings() {
        $this->db->select('testimony');
        $this->db->select('name');
        $this->db->where('approved', 1);
        $this->db->order_by('rand()');
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_elements_words.author');
        $all = $this->db->get('pmb_elements_words')->result();
        $returned = array();
        foreach($all as $a) {
            array_push($returned, $a->testimony.'<br><br>- '.$a->name.'^2000');
        }
        return $returned;
    }

    function approve($id) {
        $this->db->where('id', $id);
        $this->db->set('approved', 1);
        $this->db->update('pmb_elements_words');
    }
}
