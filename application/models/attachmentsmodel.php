<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attachmentsmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function create($post, $filename) {
        $this->db->set('post', $post);
        $this->db->set('filename', $filename);
        $this->db->set('link', 'uploads/attachments/'.$filename);
        $this->db->insert('pmb_attachments');
    }

    function isExist($post) {
        $this->db->where('post', $post);
        if ($this->db->get('pmb_attachments')->num_rows() > 0)
            return true;
        return false;
    }

    function get($postid) {
        $this->db->where('post', $postid);
        $q = $this->db->get('pmb_attachments');
        if ($q->num_rows() > 0)
            return $q->row();
        return false;
    }
}
