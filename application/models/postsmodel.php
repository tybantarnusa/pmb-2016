<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Postsmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Commentsmodel', 'comments');
		$this->load->model('Attachmentsmodel', 'attachments');
		$this->load->helper('string');
    }

    function create() {
        $author = $this->session->userdata('npm');

		$title = $this->input->post('title', TRUE);
		$category = $this->input->post('category', TRUE);
		$content = $this->input->post('content', TRUE);
		$summary = strlen(strip_tags($content)) > 200 ? substr(strip_tags($content),0,50)."..." : strip_tags($content);
		$eventtime = NULL;
		if ($category == 2 || $category == 3 || $category == 6) {
			$eventtime = $this->input->post('event-time', TRUE);
		}

		$uniqueId = random_string('alpha', 6);

		while ($this->isExist($uniqueId)) {
			$uniqueId = random_string('alpha', 6);
		}

		if (!$title || !$content) {
			$this->session->set_flashdata('error', 'Semua field harus diisi.');
			redirect(site_url('create'));
		}

		$this->db->set('id', $uniqueId);
		$this->db->set('title', $title);
		$this->db->set('author', $author);
		$this->db->set('summary', $summary);
		$this->db->set('content', $content);
		$this->db->set('type', $category);
		$this->db->set('eventtime', $eventtime);

		$this->db->insert('pmb_posts');

        return $uniqueId;
    }

    function ask() {
        $author = $this->session->userdata('npm');

		$title = $this->input->post('title', TRUE);
		$content = $this->input->post('content');

        if (!$title || !$content) {
			$this->session->set_flashdata('error', 'Semua field harus diisi.');
			redirect(site_url('ask'));
		}

        $summary = strlen(strip_tags($content)) > 200 ? substr(strip_tags($content),0,50)."..." : strip_tags($content);

		$uniqueId = random_string('alpha', 6);

		while ($this->isExist($uniqueId)) {
			$uniqueId = random_string('alpha', 6);
		}

		$this->db->set('id', $uniqueId);
		$this->db->set('title', $title);
		$this->db->set('author', $author);
		$this->db->set('summary', $summary);
		$this->db->set('content', $content);
		$this->db->set('type', 5);
		$this->db->set('eventtime', NULL);

		$this->db->insert('pmb_posts');
    }

	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('pmb_posts');
	}

    function getAll() {
        $this->db->where('type !=', 5);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        $this->db->order_by('time', 'desc');
        return $this->db->get('pmb_posts')->result();
    }

    function getQuestions() {
        $this->db->where('type', 5);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        $this->db->order_by('time', 'desc');
        return $this->db->get('pmb_posts')->result();
    }

    function get($category) {
        $this->db->where('type', $category);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        $this->db->order_by('eventtime', 'desc');
        return $this->db->get('pmb_posts')->result();
    }

	function getAssignments() {
        $this->db->where('type', 2);
        $this->db->or_where('type', 6);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        $this->db->order_by('eventtime', 'desc');
        return $this->db->get('pmb_posts')->result();
    }

    function getOne($id) {
        $this->db->where('id', $id);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        return $this->db->get('pmb_posts')->row();
    }

	function isExist($id) {
		$this->db->where('id', $id);
		return $this->db->get('pmb_posts')->num_rows() > 0;
	}

    function getFromTo($limit, $offset) {
		$this->db->where('type !=', 5);
        $this->db->order_by('time', 'desc');
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        $this->db->limit($limit, $offset);
        $arr = array();
        $posts = $this->db->get('pmb_posts')->result();
        foreach ($posts as $post) {
			$post->hasattachments = $this->attachments->isExist($post->id);
			$post->commentsnum = $this->getCommentsNum($post->id);
			array_push($arr, $post);
		}
        return $arr;
    }

    function getQuestionsFromTo($limit, $offset) {
		$this->db->where('type', 5);
        $this->db->order_by('time', 'desc');
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_posts.author');
        $this->db->limit($limit, $offset);
        $arr = array();
        $posts = $this->db->get('pmb_posts')->result();
        foreach ($posts as $post) {
			$post->hasattachments = $this->attachments->isExist($post->id);
			$post->commentsnum = $this->getCommentsNum($post->id);
			array_push($arr, $post);
		}
        return $arr;
    }

    function getCommentsNum($id) {
        return sizeof($this->comments->getInPost($id));
    }
}
