<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commentsmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function create() {
        $author = $this->session->userdata('npm');

		$comment = $this->input->post('comment', TRUE);
        $postid = $this->input->post('postid', TRUE);

		if (!$comment) return false;

        $this->db->set('post', $postid);
		$this->db->set('author', $author);
		$this->db->set('comment', $comment);

		$this->db->insert('pmb_comments');
    }

    function getInPost($postid) {
        $this->db->where('post', $postid);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_comments.author');
        $this->db->order_by('time', 'asc');
        return $this->db->get('pmb_comments')->result();
    }
}
