<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medicalmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function create() {
        $role = $this->session->userdata('role');
        if ($role == 10 || $role == 3 || $role == 7) {
            $info = $this->input->post('info', TRUE);
            $this->db->set('info', $info);
            $this->db->insert('pmb_medical_info');
        }
    }

    function delete($id) {
        $role = $this->session->userdata('role');
        if ($role == 10 || $role == 3 || $role == 7) {
            $this->db->where('id', $id);
            $this->db->delete('pmb_medical_info');
        }
        redirect(site_url('medis'));
    }

    function getAll() {
        return $this->db->get('pmb_medical_info')->result();
    }

    function getOneRandomly() {
        $this->db->select('info');
        $this->db->order_by('rand()');
        return $this->db->get('pmb_medical_info')->row();
    }
}
