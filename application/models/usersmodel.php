<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usersmodel extends CI_Model {

    public function __construct() {
        parent:: __construct();
    }

    public function register($npm, $name) {
        $num_result = $this->db->where('npm', $npm)->count_all_results('pmb_users');

        if ($num_result < 1) {
            $data = array(
                'npm' => $npm,
                'name' => $name,
            );

            if(substr($npm, 0, 2) == '16') {
                $data['role'] = 2;
            }

            $this->db->insert('pmb_users', $data);
        } else {
            $this->db->where('npm', $npm);
            $this->db->set('name', $name);
			$this->db->set('lastlogin', date("Y-m-d H:i:s", time() + 119));
            $this->db->update('pmb_users');
        }
    }

    public function get($npm) {
        $this->db->where('npm', $npm);
        return $this->db->get('pmb_users')->row();
    }

    public function getAll($sortByLogin = true, $npm = false) {
        if ($sortByLogin) {
            $this->db->order_by('lastlogin', 'desc');
        }
        if ($npm != false) {
            $this->db->like('npm', $npm, 'after');
        }
        return $this->db->get('pmb_users')->result();
    }

    public function changePic($npm, $link) {
        $this->db->where('npm', $npm);
        $this->db->set('pic', $link);
        $this->db->update('pmb_users');
    }

    public function getPic($npm) {
        $this->db->where('npm', $npm);
        return $this->db->get('pmb_users')->row()->pic;
    }
}
