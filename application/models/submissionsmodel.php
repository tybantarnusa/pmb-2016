<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submissionsmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function submit($post, $filename) {
        $currenttime = date("Y-m-d H:i:s", time() + 119);
        $user = $this->session->userdata('npm');

        $this->db->set('post', $post);
        $this->db->set('file', $filename);
        $this->db->set('time', $currenttime);

        if ($this->get($user, $post)) {
            $this->db->where('author', $user);
			$this->db->where('post', $post);
            $this->db->update('pmb_submissions');
        } else {
            $this->db->set('author', $user);
            $this->db->insert('pmb_submissions');
        }
    }

    function get($user, $postid) {
        $this->db->where('author', $user);
        $this->db->where('post', $postid);
        $q = $this->db->get('pmb_submissions');
        if ($q->num_rows() > 0)
            return $q->row();
        return false;
    }

    function getInPost($postid) {
        $this->db->where('post', $postid);
        $this->db->join('pmb_users', 'pmb_users.npm = pmb_submissions.author');
        $this->db->order_by('time', 'asc');
        return $this->db->get('pmb_submissions')->result();
    }
}
