<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
	}

	public function index()
	{
		$data['page'] = 'events';
		$data['posts'] = $this->posts->get(3);
		$this->load->view('events', $data);
	}
}
