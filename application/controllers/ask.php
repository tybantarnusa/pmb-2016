<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ask extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
	}

	public function index()
	{
		$data['page'] = 'new post';
		$this->load->view('new-question', $data);
	}

	public function hada() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->posts->ask();
		}
		redirect(site_url('forum'));
	}
}
