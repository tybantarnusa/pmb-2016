<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assignments extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
	}

	public function index()
	{
		$data['page'] = 'assignments';
		$data['posts'] = $this->posts->getAssignments();
		$this->load->view('assignments', $data);
	}
}
