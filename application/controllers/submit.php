<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submit extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Submissionsmodel', 'submissions');
	}

	public function index()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $postid = $this->input->post('postid');

            $currenttime = date("Y-m-d H:i:s", time() + 119);
            $deadline = $this->input->post('deadline');
            if ($currenttime <= $deadline) {

    			if ($_FILES['submission']['size'] > 0) {
    				$config['upload_path']   = './uploads/submissions/';
    				$config['allowed_types'] = 'pdf|zip|rar';
    				$config['max_size']      = 6000;
					$config['overwrite']	 = TRUE;
    				$this->load->library('upload', $config);

    				if (!$this->upload->do_upload('submission')) {
    					$this->session->set_flashdata('error', $this->upload->display_errors());
    				} else {
    					$data = $this->upload->data();
    					$this->submissions->submit($postid, $data['file_name']);
    				}
                }
            } else {
                $this->session->set_flashdata('error', 'Kamu telat mengumpulkan!');
            }
            redirect(site_url('post/p/'.$postid));

        } else {

            redirect(site_url());

        }
	}

}
