<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Usersmodel', 'users');
	}

	public function index()
	{
		redirect(site_url('profile/edit'));
	}

	public function edit() {
		$data['page'] = 'change display picture';
		$data['error'] = '';
		$this->load->view('edit', $data);
	}

	public function do_upload() {
		$npm = $this->session->userdata('npm');
		$config['upload_path']   = './uploads/dp/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']      = 5 * 1024;
		$config['max_width']     = 3000;
		$config['max_height']    = 2000;
		$config['encrypt_name']	 = TRUE;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')) {
			$error = array('error' => $this->upload->display_errors(), 'page' => 'change display picture');
			$this->load->view('edit', $error);
		}

		else {
			$uploadData = $this->upload->data();
			$this->users->changePic($npm, 'uploads/dp/'.$uploadData['file_name']);
			$this->session->set_userdata('dp', 'uploads/dp/'.$uploadData['file_name']);
			$data = array('upload_data' => $this->upload->data(), 'page' => 'change display picture', 'error' => '');
			$this->load->view('edit', $data);
		}
	}
}
