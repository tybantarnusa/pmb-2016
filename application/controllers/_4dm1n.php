<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _4dm1n extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Usersmodel', 'users');
	}

	public function index()
	{
		if ($this->session->userdata('role') != 10) {
			redirect('https://www.youtube.com/watch?v=Z3ZAGBL6UBA');
			return false;
		}

		$data['page'] = '4dm1n D45hb04rd';
		$data['users'] = $this->users->getAll(true);
		$this->load->view('4dm1n', $data);
	}

	public function m4b4()
	{
		if ($this->session->userdata('role') != 10) {
			redirect('https://www.youtube.com/watch?v=Z3ZAGBL6UBA');
			return false;
		}

		$data['page'] = '4dm1n D45hb04rd';
		$data['users'] = $this->users->getAll(true, 16);
		$this->load->view('4dm1n', $data);
	}
}
