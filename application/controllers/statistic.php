<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistic extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
	}

	public function index()
	{
		$data['page'] = 'statistic';
		$this->load->view('error', $data);
	}
}
