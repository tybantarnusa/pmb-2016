<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Elements extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
		$this->load->model('Elementsmodel', 'elements');
	}

	public function index()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->elements->create();
			$data['thankyou'] = 'Pesan Kamu sudah dikirimkan dan akan segera diperiksa.';
		}
		$data['page'] = 'apa kata elemen';
		$this->load->view('elements', $data);
	}
}
