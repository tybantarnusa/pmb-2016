<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
		$this->load->model('Attachmentsmodel', 'attachments');
	}

	public function index()
	{
		$role = $this->session->userdata('role');
	    if ($role == 3 || $role == 4 || $role == 5 || $role == 10) {
			$data['page'] = 'new post';
			$this->load->view('new', $data);
		} else {
			redirect(site_url());
		}
	}

	public function hada() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			
			if ($_FILES['attachment']['size'] > 0) {
				$config['upload_path']   = './uploads/attachments/';
				$config['allowed_types'] = 'pdf|zip|rar|7z';
				$config['max_size']      = 2048;
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('attachment')) {
					$this->session->set_flashdata('error', $this->upload->display_errors());
					redirect(site_url('create'));
				} else {
					$data['upload_data'] = $this->upload->data();
					$post = $this->posts->create();

					$this->attachments->create($post, $data['upload_data']['file_name']);

					redirect(site_url());
				}
			} else {
				$this->posts->create();
				redirect(site_url());
			}

		}
	}
}
