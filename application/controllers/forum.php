<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
		$this->load->model('Commentsmodel', 'comments');
	}

	public function index()
	{
		$data['page'] = 'forum';
		$data['posts'] = array();
		$posts = $this->posts->getQuestions();
		foreach ($posts as $post) {
			$post->commentsnum = $this->posts->getCommentsNum($post->id);
			array_push($data['posts'], $post);
		}
		$this->load->view('forum', $data);
	}

	public function q($id = NULL) {
		if ($id == NULL) {
			redirect(site_url('forum'));
			return;
		}

		if ($id == 'comment') {
			$this->comment();
			return;
		}

		$data['page'] = 'forum';
		$data['post'] = $this->posts->getOne($id);
		$data['postId'] = $id;
		$data['comments'] = $this->comments->getInPost($id);
		$this->load->view('single', $data);
	}

	private function comment() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->comments->create();
		}
	}
}
