<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function e404()
	{
		$data['page'] = 'error 404';
		$this->load->view('error_404', $data);
	}
}
