<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
		$this->load->model('Elementsmodel', 'elements');
	}

	public function index()
	{
		redirect(site_url());
	}

	public function posts($offset = null) {
        $role = $this->session->userdata('role');
        if ($role && $role != 1) {
    		if ($this->input->server('REQUEST_METHOD') == 'POST') {
    			if ($offset == null) {
    				echo json_encode($this->posts->getAll());
    			} else {
    				$posts = $this->posts->getFromTo(6, $offset * 5);
    				echo json_encode($posts);
    			}
    		} else {
    			redirect(site_url());
    		}
        } else {
            echo "flag{1n!_buK4n_cTF_s1h}";
        }
	}

    public function questions($offset = null) {
        $role = $this->session->userdata('role');
        if ($role && $role != 1) {
    		if ($this->input->server('REQUEST_METHOD') == 'POST') {
    			if ($offset == null) {
    				echo json_encode($this->posts->getQuestions());
    			} else {
    				$posts = $this->posts->getQuestionsFromTo(6, $offset * 5);
    				echo json_encode($posts);
    			}
    		} else {
    			redirect(site_url());
    		}
        } else {
            echo "flag{1n!_buK4n_cTF_s1h}";
        }
	}

	public function elements($adminmode = false) {
		$role = $this->session->userdata('role');
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if ($adminmode == 'AiCarGattIg' && ($role == 3 || $role == 10)) {
				echo json_encode($this->elements->getAll());
			} else if ($adminmode == 'mantapjiwa' && ($role == 3 || $role == 10)) {
				$id = $this->input->post('id');
				$this->elements->approve($id);
			} else {
				echo json_encode($this->elements->getApproved());
			}
		}
	}

	public function elementstrings() {
		echo json_encode($this->elements->getApprovedStrings());
	}

	public function calendar() {
		$assignments = $this->posts->getAssignments();
		$events = $this->posts->get(3);

		$calendarObj = array();

		foreach($assignments as $assignment) {
			$obj->id = $assignment->id;
			$obj->title = $assignment->title;
			$obj->start = $assignment->eventtime;
			$obj->url = site_url('post/p/'.$assignment->id);
			$obj->color = '#f77e8c';
			array_push($calendarObj, $obj);
			$obj = NULL;
		}

		foreach($events as $event) {
			$obj->id = $event->id;
			$obj->title = $event->title;
			$obj->start = $event->eventtime;
			$obj->url = site_url('post/p/'.$event->id);
			$obj->color = '#3dd6f5';
			array_push($calendarObj, $obj);
			$obj = NULL;
		}

		echo json_encode($calendarObj);
	}
}
