<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Usersmodel', 'users');
	}

	public function index() {
		if ($this->session->userdata('logged')) redirect(site_url('login/sso'));
		$this->load->view('login');
	}

	public function sso() {

		$logged = SSO\SSO::check();

		if (!$logged) {

			SSO\SSO::authenticate();

		} else {

			$user = SSO\SSO::getUser();
			$npm = $user->npm;
			$name = $user->name;
			$username = $user->username;

			$this->users->register($npm, $name);

			$this->session->set_userdata('logged', TRUE);
			$this->session->set_userdata('npm', $npm);
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('name', $name);

			$meInDatabase = $this->users->get($npm);
			$this->session->set_userdata('dp', $meInDatabase->pic);
			$this->session->set_userdata('role', $meInDatabase->role);

			redirect(site_url());

		}
	}

}
