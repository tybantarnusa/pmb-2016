<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
		$this->load->model('Commentsmodel', 'comments');
		$this->load->model('Attachmentsmodel', 'attachments');
		$this->load->model('Submissionsmodel', 'submissions');
	}

	public function index()
	{
		redirect(site_url());
	}

	public function p($id = NULL) {
		if ($id == NULL) {
			redirect(site_url());
			return;
		}

		if ($id == 'comment') {
			$this->comment();
			return;
		}

		$data['page'] = 'post';
		$data['post'] = $this->posts->getOne($id);
		$data['post']->attachment = $this->attachments->get($id);
		$data['postId'] = $id;
		$data['comments'] = $this->comments->getInPost($id);
		$data['submission'] = $this->submissions->get($this->session->userdata('npm'), $id);
		$data['post_submissions'] = $this->submissions->getInPost($id);
		$this->load->view('single', $data);
	}

	public function delete($id = NULL, $seriously = false) {
		if ($id == NULL) {
			redirect(site_url());
			return false;
		}

		$data['page'] = 'Confirm Delete';
		$data['post'] = $this->posts->getOne($id);
		$role = $this->session->userdata('role');

		if ($role == 2) {
			redirect(site_url());
			return false;
		}

		if ($data['post']->author != $this->session->userdata('npm') && $role != 3 && role != 10) {
			redirect(site_url());
			return false;
		}

		if (!$seriously) {
			$this->load->view('confirmdelete', $data);
			return false;
		}

		$this->posts->delete($id);
		redirect(site_url());
		return true;
	}

	private function comment() {
		$role = $this->session->userdata('role');
		if ($role && $role != 1) {
			if ($this->input->server('REQUEST_METHOD') == 'POST') {
				$this->comments->create();
			}
		}
	}
}
