<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Postsmodel', 'posts');
		$this->load->model('Attachmentsmodel', 'attachments');
	}

	public function index()
	{
		$data['page'] = 'home';
		$data['posts'] = array();
		$posts = $this->posts->getAll();
		foreach ($posts as $post) {
			$post->commentsnum = $this->posts->getCommentsNum($post->id);
			$post->hasattachments = $this->attachments->isExist($post->id);
			array_push($data['posts'], $post);
		}
		$this->load->view('home', $data);
	}
}
