<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medis extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Medicalmodel', 'medic');
	}

	public function index()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->medic->create();
		}

		$role = $this->session->userdata('role');
		if ($role == 10 || $role == 3 || $role == 7) {
			$data['page'] = 'Tambah Info Medis';
			$data['infos'] = $this->medic->getAll();
			$this->load->view('medis', $data);
		} else {
			redirect(site_url());
		}
	}

	public function delete($id) {
		$this->medic->delete($id);
	}
}
